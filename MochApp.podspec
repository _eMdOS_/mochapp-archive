Pod::Spec.new do |spec|
  spec.name = "MochApp"
  spec.version = "0.2.0"
  spec.summary = "A swifty micro-framework."
  spec.description = <<-DESC
                      A swifty micro-framework that enables utilities for making easier to develop the app core from scratch.
                    DESC
  spec.homepage = "https://gitlab.com/_eMdOS_/mochapp.git"
  spec.license = "MIT"
  spec.author = "eMdOS"
  spec.swift_version = '5.0'
  spec.ios.deployment_target = "11.0"
  spec.tvos.deployment_target = "11.0"
  spec.source = { :git => "https://gitlab.com/_eMdOS_/mochapp.git", :tag => "#{spec.version}" }
  spec.source_files = "framework/sources/core/**/*.swift"
end

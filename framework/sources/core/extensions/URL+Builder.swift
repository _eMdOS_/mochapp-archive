import Foundation

// MARK: - URL.Scheme

extension URL {
    public enum Scheme: String {
        case http
        case https
    }
}

// MARK: - URL

extension URL {
    public class Builder {
        private var components: URLComponents = .init()
        
        /// Initializes the builder with all components undefined.
        public init() {}
    }
}

public extension URL.Builder {
    /// Sets the scheme subcomponent of the URL.
    /// - Parameter scheme: scheme
    func setScheme(_  scheme: URL.Scheme) -> URL.Builder {
        components.scheme = scheme.rawValue
        return self
    }
    
    /// Sets the host subcomponent.
    /// - Parameter host: host
    func setHost(_  host: String) -> URL.Builder {
        components.host = host
        return self
    }
    
    /// Sets the port subcomponent.
    /// - Parameter port: port
    func setPort(_  port: Int) -> URL.Builder {
        components.port = port
        return self
    }
    
    /// Sets the path subcomponent.
    /// - Parameter path: path
    func setPath(_ path: String) -> URL.Builder {
        components.path = path
        return self
    }
    
    /// Sets a  single key-value pair from the query portion of a URL.
    /// - Parameter key: key
    /// - Parameter value: value
    func setQueryParam(key: String, value: Any) -> URL.Builder {
        if components.query == .none {
            components.queryItems = []
        }
        let queryItem = URLQueryItem(name: key, value: String(describing: value))
        components
            .queryItems?
            .append(queryItem)
        return self
    }
    
    /// Returns the built URL from the components set using the builder's functions.
    /// When the URL cannot be built, the functions throws a `URL.Builder.Error.creatingURL`
    func build() throws -> URL {
        guard let url = components.url else {
            throw URL.Builder.Error.creatingURL
        }
        return url
    }
}

// MARK: - URL.Builder.Error

extension URL.Builder {
    public enum Error: Swift.Error, Equatable {
        case creatingURL
    }
}

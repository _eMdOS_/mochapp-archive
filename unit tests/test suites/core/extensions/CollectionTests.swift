import XCTest
@testable import MochApp

final class CollectionTests: XCTestCase {
    let intArray: [Int] = [0, 1, 2]
}

// MARK: - Test Cases

extension CollectionTests {
    func test_safeAccessToAllIndices() {
        intArray
            .enumerated()
            .forEach { (index, value) in
                XCTAssertEqual(index, intArray.safe[value])
            }
    }

    func test_nilWhenIndexOutOfBounds() {
        XCTAssertNil(intArray.safe[intArray.endIndex])
    }

    func test_safeAccessByClosedRange() {
        let closedRanges: [ClosedRange<Int>] = [0...1, 0...2]
        closedRanges
            .forEach { closedRange in
                XCTAssertNotNil(intArray.safe[closedRange])
            }
    }

    func test_nilWhenClosedRangeOutOfBounds() {
        XCTAssertNil(intArray.safe[0...3])
    }

    func test_safeAccessByRange() {
        let closedRanges: [Range<Int>] = [0..<1, 0..<2, 0..<3]
        closedRanges
            .forEach { closedRange in
                XCTAssertNotNil(intArray.safe[closedRange])
            }
    }

    func test_nilWhenRangeOutOfBounds() {
        XCTAssertNil(intArray.safe[0..<4])
    }
}

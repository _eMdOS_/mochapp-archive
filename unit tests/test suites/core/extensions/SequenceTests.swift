import XCTest
@testable import MochApp

final class SequenceTests: XCTestCase {
    func test_map() {
        // given
        let users: [User] = [User(id: "A"), User(id: "B")]
        // when
        let userIDs = users.map(\.id)
        // then
        XCTAssertEqual(["A", "B"], userIDs)
    }

    func test_sorted() {
        // given
        let users: [User] = [User(id: "D"), User(id: "B"), User(id: "C"), User(id: "A")]
        // when
        let userIDs = users
            .sorted(by: \.id)
            .map(\.id)
        // then
        XCTAssertEqual(["A", "B", "C", "D"], userIDs)
    }

    func test_sorted_ascending() {
        // given
        let users: [User] = [User(id: "D"), User(id: "B"), User(id: "C"), User(id: "A")]
        // when
        let userIDs = users
            .sorted(by: \.id, using: <)
            .map(\.id)
        // then
        XCTAssertEqual(["A", "B", "C", "D"], userIDs)
    }

    func test_sorted_descending() {
        // given
        let users: [User] = [User(id: "D"), User(id: "B"), User(id: "C"), User(id: "A")]
        // when
        let userIDs = users
            .sorted(by: \.id, using: >)
            .map(\.id)
        // then
        XCTAssertEqual(["D", "C", "B", "A"], userIDs)
    }

    func test_grouped() {
        // given
        let users: [User] = [User(id: "A"), User(id: "B"), User(id: "C"), User(id: "A")]
        // when
        let groups = users.grouped(by: \.id)
        // then
        XCTAssertEqual(2, groups["A"]?.count)
        XCTAssertEqual(1, groups["B"]?.count)
        XCTAssertEqual(1, groups["C"]?.count)
    }
}

private extension SequenceTests {
    struct User: Hashable {
        let id: String
    }
}

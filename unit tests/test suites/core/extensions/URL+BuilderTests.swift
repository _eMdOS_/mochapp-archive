import XCTest
import MochApp

final class URLBuilderTests: XCTestCase {
    func test_build_successful() throws {
        let url = try URL.Builder()
            .setScheme(.https)
            .setHost("api.test.com")
            .setPort(80)
            .setPath("/movies")
            .setQueryParam(key: "sort", value: "ascending")
            .setQueryParam(key: "page_size", value: 20)
            .build()
        
        XCTAssertEqual("https://api.test.com:80/movies?sort=ascending&page_size=20", url.absoluteString)
    }
    
    func test_build_error() throws {
        let builder = URL.Builder()
            .setScheme(.https)
            .setHost("api.test.com")
            .setPort(80)
            .setPath("movies") // error -> wrong path component
        
        XCTAssertExpression(try builder.build(), throws: URL.Builder.Error.creatingURL)
    }
}
